package com.appstudio.robin.resourcevalidator

import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys

class DirectoryConvertAction : AnAction() {

    override fun actionPerformed(e: AnActionEvent) {
        val file = e.getData(CommonDataKeys.VIRTUAL_FILE) ?: return
        val resDir = file.findParentResDirectory() ?: return

        val changeCount = ResourceNameConverter.convertFiles(resDir)

        if (changeCount == 0) {
            showNotification(e, "No files changed", "All resource names are valid.")
        } else {
            val message = "Updated $changeCount resource file name${if (changeCount > 1) "s" else ""}."
            showNotification(e, "File names updated", message)
        }
    }

    override fun update(e: AnActionEvent) {
        val file = e.getData(CommonDataKeys.VIRTUAL_FILE)
        e.presentation.isEnabledAndVisible = file != null && file.findParentResDirectory() != null
    }
}
