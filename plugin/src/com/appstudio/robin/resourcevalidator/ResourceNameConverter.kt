package com.appstudio.robin.resourcevalidator

import com.intellij.openapi.application.WriteAction
import com.intellij.openapi.ui.Messages
import com.intellij.openapi.vfs.VirtualFile
import java.io.IOException
import java.util.regex.Pattern

fun VirtualFile.isResourceFile(): Boolean {
    return !isDirectory && findParentResDirectory() != null
}

fun VirtualFile.isResourceDirectory(): Boolean {
    return isDirectory && findParentResDirectory() != null
}

fun VirtualFile.findParentResDirectory(): VirtualFile? {
    var dir = this
    for (i in 0..2) {
        if (dir.isDirectory && dir.name == "res") {
            return dir
        }
        dir = dir.parent ?: break
    }
    return null
}

object ResourceNameConverter {

    private val VALID = Pattern.compile("^[a-z0-9_.]*$")

    fun isValidResourceName(name: String): Boolean {
        return VALID.matcher(name).matches()
    }

    fun convertFiles(files: Array<VirtualFile>): Int {
        var changeCount = 0
        for (file in files) {
            if (file.isResourceFile() && !isValidResourceName(file.name)) {
                renameFile(file, convertFileName(file.name))
                changeCount++
            }
        }
        return changeCount
    }

    fun convertFiles(root: VirtualFile): Int {
        var changeCount = 0

        fun convertFile(file: VirtualFile) {
            if (file.isResourceDirectory()) {
                file.children.forEach(::convertFile)
            }

            if (file.isResourceFile() && !isValidResourceName(file.name)) {
                renameFile(file, convertFileName(file.name))
                changeCount++
            }
        }
        convertFile(root)
        return changeCount
    }

    private fun renameFile(file: VirtualFile, newName: String) {
        try {
            WriteAction.run<IOException> { file.rename(this, newName) }
        } catch (ex: IOException) {
            val message = "An exception occurred while renaming file '${file.name}':\n$ex"
            Messages.showErrorDialog(message, "Error")
        }
    }

    private fun convertFileName(name: String): String {
        if (isValidResourceName(name)) {
            return name
        }

        val result = StringBuilder()
        for (i in 0 until name.length) {
            var c = name[i]

            if (Character.isUpperCase(c)) {
                c = Character.toLowerCase(c)
            }

            if (c == '-' || c == ' ') {
                c = '_'
            }

            if (VALID.matcher(c.toString()).matches()) {
                result.append(c)
            }
        }

        return result.toString()
    }
}
