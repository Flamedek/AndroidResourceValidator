package com.appstudio.robin.resourcevalidator

import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys

class FileConvertAction : AnAction() {

    override fun actionPerformed(e: AnActionEvent) {
        val files = e.getData(CommonDataKeys.VIRTUAL_FILE_ARRAY) ?: return
        val changeCount = ResourceNameConverter.convertFiles(files)

        val message = when {
            changeCount > 1 -> "$changeCount files updated."
            changeCount == 1 -> "File name updated."
            else -> "File name already valid."
        }
        showStatus(e, message)
    }

    override fun update(e: AnActionEvent) {
        val files = e.getData(CommonDataKeys.VIRTUAL_FILE_ARRAY)
        val mainFile = e.getData(CommonDataKeys.VIRTUAL_FILE)
        if (files == null || mainFile == null || !mainFile.isResourceFile() || ResourceNameConverter.isValidResourceName(mainFile.name)) {
            e.presentation.isEnabledAndVisible = false
            return
        }
        val count = e.getData(CommonDataKeys.VIRTUAL_FILE_ARRAY)?.size ?: 1
        e.presentation.text = "Fix Resource Name" + if (count > 1) "s" else ""
        e.presentation.isEnabledAndVisible = true
    }

}
