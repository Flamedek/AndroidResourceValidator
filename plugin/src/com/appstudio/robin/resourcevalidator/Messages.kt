package com.appstudio.robin.resourcevalidator

import com.intellij.notification.Notification
import com.intellij.notification.NotificationType
import com.intellij.notification.Notifications
import com.intellij.notification.NotificationsManager
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.openapi.wm.WindowManager


private const val NOTIFICATION_GROUP_ID = "Resource Name Validate Result"

fun showNotification(e: AnActionEvent, title: String, message: String) {
    val project = e.getData(CommonDataKeys.PROJECT)

    val notification = Notification(NOTIFICATION_GROUP_ID, title, message, NotificationType.INFORMATION)
    Notifications.Bus.notify(notification, project)
    NotificationsManager.getNotificationsManager().expire(notification)
}

fun showStatus(e: AnActionEvent, message: String) {
    val project = e.getData(CommonDataKeys.PROJECT)
    WindowManager.getInstance().getStatusBar(project).info = message
}